package com.template.flows

import co.paralleluniverse.fibers.Suspendable
import com.template.contracts.LoanRequestContract
import com.template.states.LoanRequestState
import net.corda.core.contracts.Command
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.node.StatesToRecord
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder


// *********
// * Flows *
// *********
@InitiatingFlow
@StartableByRPC
class SyndicateLoanApproveFlow(val status: Int,
                               val loanId : UniqueIdentifier,
                               val banks : List<Party>) : FlowLogic<SignedTransaction>() {

    @Suspendable
    override fun call() : SignedTransaction {
        // We retrieve the notary identity from the network map.
        val LoanAndRef  = serviceHub.vaultService
                .queryBy<LoanRequestState>(QueryCriteria.LinearStateQueryCriteria(linearId = listOf(loanId)))
                .states.single()
        val LoanReq = LoanAndRef.state.data

        // We create the transaction components.
        val banksPublicKey = banks.map { it.owningKey }
        val allSigners = banksPublicKey.plus(arrayOf(ourIdentity.owningKey,LoanReq.requester.owningKey))
        val command = Command(LoanRequestContract.Commands.ApproveSyndicate(), allSigners)

        print(allSigners)
        // We create a transaction builder and add the components.
        val txBuilder = TransactionBuilder(LoanAndRef.state.notary)
                .addInputState(LoanAndRef)
                .addOutputState(LoanReq.copy(bank = ourIdentity, loanStatus = status), LoanRequestContract.ID)
                .addCommand(command)

        // Verifying the transaction.
        txBuilder.verify(serviceHub)

        // Signing the transaction.
        val signedTx = serviceHub.signInitialTransaction(txBuilder)

        // Creating a session with the other party.
        val otherPartySessions = banks.map { initiateFlow(it) }
        val allotherPartySessions = otherPartySessions.plus(initiateFlow(LoanReq.requester))

        print(allotherPartySessions)
        // Obtaining the counterparty's signature.
        val fullySignedTx = subFlow(CollectSignaturesFlow(signedTx, allotherPartySessions, CollectSignaturesFlow.tracker()))

        // Finalising the transaction.
        return subFlow(FinalityFlow(fullySignedTx, allotherPartySessions))
    }

}

@InitiatedBy(SyndicateLoanApproveFlow::class)
class SyndicateLoanApproveFlowResponder(val counterpartySession: FlowSession) : FlowLogic<Unit>() {
    @Suspendable
    override fun call() {
        val signTransactionFlow = object : SignTransactionFlow(counterpartySession) {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
                val output = stx.tx.outputs.single().data
                "This must be a loan request transaction." using (output is LoanRequestState)
                val loanRequest = output as LoanRequestState
                "The loan value should be grater than zero." using (loanRequest.requestValue > 0)
            }
        }

        val expectedTxId = subFlow(signTransactionFlow).id

        subFlow(ReceiveFinalityFlow(counterpartySession, expectedTxId, StatesToRecord.ALL_VISIBLE))
    }
}
