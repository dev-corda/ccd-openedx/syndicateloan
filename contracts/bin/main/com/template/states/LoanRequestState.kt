package com.template.states

import com.template.contracts.LoanRequestContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.Party

// *********
// * State *
// *********
@BelongsToContract(LoanRequestContract::class)
data class LoanRequestState(val requestValue: Int,
                       val requester: Party,
                       val bank: Party,
                       val loanStatus: Int = 0,
                       val attachemntHash: SecureHash,
                       override val linearId: UniqueIdentifier = UniqueIdentifier()) : LinearState {
    override val participants get() = listOf(requester, bank)
}