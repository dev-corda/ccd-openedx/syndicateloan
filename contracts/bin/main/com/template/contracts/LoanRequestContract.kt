package com.template.contracts

import com.template.states.LoanRequestState
import net.corda.core.contracts.CommandData
import net.corda.core.contracts.Contract
import net.corda.core.contracts.Requirements.using
import net.corda.core.transactions.LedgerTransaction

// ************
// * Contract *
// ************
class LoanRequestContract : Contract {
    companion object {
        // Used to identify our contract when building a transaction.
        const val ID = "com.template.contracts.LoanRequestContract"
    }

    // A transaction is valid if the verify() function of the contract of all the transaction's input
    // and output states does not throw an exception.
    override fun verify(tx: LedgerTransaction) {
        // Verification logic goes here.
        val command = tx.commands.first()

        if (command.value is Commands.RequestSyndicate) {
            "No inputs should be consumed when requesting loan." using (tx.inputs.isEmpty())
            "There should be only one output state." using (tx.outputs.size == 1)
            val output = tx.outputs.single().data
            "This must be a loan request transaction." using (output is LoanRequestState)
            val loanRequest = output as LoanRequestState
            "The loan value should be grater than 9 Crore." using (loanRequest.requestValue > 900000000)
        }
        else if (command.value is Commands.ApproveSyndicate) {
            "There should be only one output state." using (tx.inputs.size == 1)
            "There should be only one output state." using (tx.outputs.size == 1)
            val output = tx.outputs.single().data
            "This must be a loan request transaction." using (output is LoanRequestState)
            val loanRequest = output as LoanRequestState
            "The loan value should be grater than 9 Crore." using (loanRequest.requestValue > 900000000)
        }

    }

    // Used to indicate the transaction's intent.
    interface Commands : CommandData {
        class RequestSyndicate : Commands
        class ApproveSyndicate : Commands
    }
}
