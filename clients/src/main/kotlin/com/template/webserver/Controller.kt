package com.template.webserver

import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import com.template.flows.LoanRequestFlow
import com.template.flows.SyndicateLoanApproveFlow
import com.template.states.LoanRequestState
import net.corda.client.jackson.JacksonSupport
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.CordaX500Name
import net.corda.core.messaging.startFlow
import net.corda.core.messaging.vaultQueryBy
import net.corda.core.node.services.AttachmentId
import net.corda.core.node.services.Vault
import net.corda.core.node.services.vault.QueryCriteria
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStream
import java.util.*
import java.util.UUID.fromString
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

/**
 * Define your API endpoints here.
 */
@RestController
@RequestMapping("/") // The paths for HTTP requests are relative to this base path.
class Controller(rpc: NodeRPCConnection) {

    companion object {
        private val logger = LoggerFactory.getLogger(RestController::class.java)
    }

    private val proxy = rpc.proxy

    @GetMapping(value = ["/getPeers"], produces = arrayOf("application/json"))
    private fun getPeers(): String {
        val objectMapper = JacksonSupport.createDefaultMapper(proxy)
        val nodeInfo = proxy.nodeInfo()

        val bankName = proxy.networkMapSnapshot()
                .map { it.legalIdentities.first() }
                .filter { it.name.toString().contains("Bank")}
        return objectMapper.writeValueAsString(bankName)
    }

    @GetMapping(value = "/getNonHistoricUUID", produces = arrayOf("application/json"))
    private fun getNonHistoricUUID(): String {
        val objectMapper = JacksonSupport.createDefaultMapper(proxy)
        val unconsumedCriteria = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.UNCONSUMED)
        val loanRequests = proxy.vaultQueryBy<LoanRequestState>(unconsumedCriteria).states.map { it.state.data }
        return objectMapper.writeValueAsString(loanRequests)
    }

    @PostMapping(value = ["/approveRequest"], headers = arrayOf("Content-Type=application/x-www-form-urlencoded"), produces = arrayOf("application/json"))
    private fun approveRequest(@RequestParam("value") uuid: String, @RequestParam("partyName") bank: List<String>): String {
        val objectMapper = JacksonSupport.createDefaultMapper(proxy)
        val x500Name = bank.map {  CordaX500Name.parse(it) }
        val otherParty = x500Name.map { proxy.wellKnownPartyFromX500Name(it)!! }
        val flowHandle = proxy.startFlow(::SyndicateLoanApproveFlow, 1, UniqueIdentifier.fromString(uuid), otherParty)
        val transactionID = flowHandle.returnValue.toCompletableFuture().get()
        return objectMapper.writeValueAsString("Success" + transactionID)
    }

    @GetMapping(value = "/myRequest", produces = arrayOf("application/json"))
    private fun myRequest(): String {
        val objectMapper = JacksonSupport.createDefaultMapper(proxy)
        val loanRequests = proxy.vaultQuery(LoanRequestState::class.java)
                .states.map { it.state.data }
        return objectMapper.writeValueAsString(loanRequests)
    }

    @RequestMapping(value = "/createSyndicateRequest", method = [RequestMethod.POST])
    @ResponseBody
    fun uploadFile(
            @RequestParam("file") file: MultipartFile,
            @RequestParam("value") requestValue: String,
            @RequestParam("bank") bank: String
    ): String? {

        try {
            // Handle the received file here
            val filename = file.originalFilename
            println("filename: "+filename)
            val hash: SecureHash = if (!(file.contentType == "application/zip" || file.contentType == "application/jar")) {
                uploadZip(file.inputStream, filename!!)

            } else {
                proxy.uploadAttachmentWithMetadata(
                        jar = file.inputStream,
                        uploader = "Node",
                        filename = filename!!
                )
            }
            println("hash of file "+hash)

            val objectMapper = JacksonSupport.createDefaultMapper(proxy)
            val x500Name = CordaX500Name.parse(bank)
            val pbank =  proxy.wellKnownPartyFromX500Name(x500Name)!!
            val ivalue = Integer.parseInt(requestValue)
            val flowHandle = proxy.startFlow(::LoanRequestFlow, ivalue, pbank, hash)
            val transactionID = flowHandle.returnValue.toCompletableFuture().get()

            println("Transaction succsess. Tn ID:"+transactionID)

            return objectMapper.writeValueAsString("Success" + transactionID)
        } catch (e: Exception) {
            println( ResponseEntity<Any>(HttpStatus.BAD_REQUEST))
        }
        return null
    }

    private fun uploadZip(inputStream: InputStream, filename: String): AttachmentId {
        val zipName = "$filename-${UUID.randomUUID()}.zip"
        FileOutputStream(zipName).use { fileOutputStream ->
            ZipOutputStream(fileOutputStream).use { zipOutputStream ->
                val zipEntry = ZipEntry(filename)
                zipOutputStream.putNextEntry(zipEntry)
                inputStream.copyTo(zipOutputStream, 1024)
            }
        }
        println("uploaded file:"+zipName)
        return FileInputStream(zipName).use { fileInputStream ->
            val hash = proxy.uploadAttachmentWithMetadata(
                    jar = fileInputStream,
                    uploader = "Node",
                    filename = filename
            )
            hash
        }
    }
}
